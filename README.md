# PSK-TOOLS - POWERSHELL (STYRKESKAL)

This is a powershell repo for automation tools of all sorts

## Generate CHANGELOG.md

1. Install: <https://pypi.org/project/gitlab-changelog-generator/>

2. changegen --ip <https://gitlab.com> --group psktools --project powershell --branches master release/vX.X.X --version X.X.X

3. Merge content from genreated file into CHANGELOG.md

4. Delete CHANGELOG_generated.md

## Sources

<https://pypi.org/project/gitlab-changelog-generator/>
