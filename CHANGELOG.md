# CHANGELOG

## v1.2.5 (2020-01-12)

* 2020-01-12 - Actually add the crontab change this commit

## v1.2.3 (2020-01-12)

* 2020-01-12 - Set up git flow init script to use the correct way of pushing after running on a blank repo

## v1.2.2 (2020-01-12)

* 2020-01-12 - Put links in bracket things

## v1.2.1 (2020-01-12)

* 2020-01-12 - Added more stuff to general readme, added stub changelog, and guide on how to generate changelog, and cleaned some stuff

* 2020-01-12 - Updated documentation
