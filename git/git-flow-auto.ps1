#! /usr/bin/pwsh
#requires -version 5
<#
.SYNOPSIS
    This script was made to make gitflow work flow much simpler
.DESCRIPTION
    It wraps "git flow" commands
.PARAMETER type
    which release type we're using
.PARAMETER action
    What we're doing with that release
.PARAMETER name
    The name of the branch, release or hotfix etc.
.INPUTS
    None
.OUTPUTS
    None
.NOTES
    Version:        1.0.2
    Author:         Philip Skov
    Creation Date:  2019-01-12
    Purpose/Change: Initial script development
.EXAMPLE
    .\git-flow-auto.ps1 -type feature -action start -name new-feature-name
.EXAMPLE
    .\git-flow-auto.ps1 -type feature -action finish -name new-feature-name
#>

param(
    [Parameter(Mandatory)][ValidateNotNullorEmpty()][string]$type,
    [Parameter(Mandatory)][ValidateNotNullorEmpty()][string]$action,
    [Parameter(Mandatory)][ValidateNotNullorEmpty()][string]$name
)

#-----------------------------------------------------------[Functions]------------------------------------------------------------
function _start {
    
    git flow $type start $name
}

function _finish {
    
    git flow $type finish $name
    git push
    
    if ($type -notlike 'feature')
    {

        git push --tags
        git checkout master
        git push
        git checkout develop

    }

}

function _track {

    git flow $type track $name

}

function _pull {
    
    if ($type -like 'feature')
    {
        
        git flow $type pull $name
    
    }

}

function _publish {
    
    git flow $type publish $name
    
}

#-----------------------------------------------------------[Execution]------------------------------------------------------------

if ($action -like "start")
{

    _start
    _publish

}

if ($action -like "finish")
{

    _finish

}

if ($action -like "track")
{

    _track

}

if ($action -like "pull")
{

    _pull

}

if ($action -like "publish")
{

    _publish

}
