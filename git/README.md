# IMPORTANT

Merge files from feature branch into develop before commit, if develop does not have the files

## NOTES

I have created local dedicated folder to check out the latest tag of this repo.
I develop the project from another folder, to keep my alias' as stable as possible.

## Installation guide (Linux)

1. Change $SCRIPT_PATH variable in alias.sh and cron.sh

2. Run ./setup/linux/install-"OS".sh

3. Run ./setup/linux/alias.sh

4. Run ./setup/linux/cron.sh

## Alias list (Linux)

* ToDo

## Sources
  
<https://stackoverflow.com/questions/878600/how-to-create-a-cron-job-using-bash-automatically-without-the-interactive-editor>

<https://www.phillipsj.net/posts/using-powershell-scripts-from-bash/>

<https://gist.github.com/9to5IT/9620683>

<https://danielkummer.github.io/git-flow-cheatsheet/>

<https://stackoverflow.com/questions/17414104/git-checkout-latest-tag>

<https://stackoverflow.com/questions/5573683/adding-alias-to-end-of-alias-list-in-bashrc-file-using-sed>
