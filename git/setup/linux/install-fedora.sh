#!/usr/bin/bash
# install git and git flow
sudo dnf -y git gitflow
# Register the Microsoft signature key
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc

# Register the Microsoft RedHat repository
curl https://packages.microsoft.com/config/rhel/7/prod.repo | sudo tee /etc/yum.repos.d/microsoft.repo

# Update the list of products
sudo dnf update

# Install a system component
sudo dnf install -y compat-openssl10

# Install PowerShell
sudo dnf install -y powershell
