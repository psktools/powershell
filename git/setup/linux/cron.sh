#!/usr/bin/bash
## DECLARATIONS ##
CRON_PATH="/tmp/${USER}_cron"
SCRIPT_PATH="${HOME}/scripts/git/powershell/git/setup/linux/checkout-latest-tag.sh"

# write out current crontab
crontab -l > $CRON_PATH
# echo new cron into cron file
echo "00 09 * * *  cd $SCRIPT_PATH; sh $SCRIPT_PATH" >> $CRON_PATH
# install new cron file
crontab $CRON_PATH
# remove it again
rm $CRON_PATH
