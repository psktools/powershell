#!/usr/bin/bash

# DELCARATIONS

SCRIPT_PATH="${HOME}/scripts/git/powershell/git"

## STANDARD GIT COMMANDS, BUT BETTER ##
    # git commit
echo "alias g-commit='$SCRIPT_PATH/git-commit.ps1'" >> $HOME/.bashrc
    # git push
echo "alias g-push='$SCRIPT_PATH/git-push.ps1'" >> $HOME/.bashrc
    # git stage, commit and push
echo "alias g-compush='g-commit && g-push'" >> $HOME/.bashrc

## INIT REPO FOR GIT FLOW ##
echo "alias gf-init='$SCRIPT_PATH/git-flow-init.ps1'" >> $HOME/.bashrc

## START SECTION ##
    # git flow start feature
echo "alias gf-feature-start='$SCRIPT_PATH/git-flow-auto.ps1 -type feature -action start -name'" >> $HOME/.bashrc
    # git flow start release
echo "alias gf-release-start='$SCRIPT_PATH/git-flow-auto.ps1 -type release -action start -name'" >> $HOME/.bashrc
    # git flow start release
echo "alias gf-release-start='$SCRIPT_PATH/git-flow-auto.ps1 -type release -action start -name'" >> $HOME/.bashrc
    # git flow start hotfix
echo "alias gf-hotfix-start='$SCRIPT_PATH/git-flow-auto.ps1 -type hotfix -action start -name'" >> $HOME/.bashrc

## FINISH SECTION ##
    # git flow finish feature
echo "alias gf-feature-finish='$SCRIPT_PATH/git-flow-auto.ps1 -type feature -action finish -name'" >> $HOME/.bashrc
    # git flow finish release
echo "alias gf-release-finish='$SCRIPT_PATH/git-flow-auto.ps1 -type release -action finish -name'" >> $HOME/.bashrc
    # git flow finish release
echo "alias gf-release-finish='$SCRIPT_PATH/git-flow-auto.ps1 -type release -action finish -name'" >> $HOME/.bashrc
    # git flow finish hotfix
echo "alias gf-hotfix-finish='$SCRIPT_PATH/git-flow-auto.ps1 -type hotfix -action finish -name'" >> $HOME/.bashrc

## PULL SECTION ##
    # git flow pull feature
echo "alias gf-feature-pull='$SCRIPT_PATH/git-flow-auto.ps1 -type feature -action pull -name'" >> $HOME/.bashrc

## TRACK SECTION ##
    # git flow track feature
echo "alias gf-feature-track='$SCRIPT_PATH/git-flow-auto.ps1 -type feature -action track -name'" >> $HOME/.bashrc
    # git flow track release
echo "alias gf-release-track='$SCRIPT_PATH/git-flow-auto.ps1 -type release -action track -name'" >> $HOME/.bashrc
    # git flow track hotfix
echo "alias gf-hotfix-track='$SCRIPT_PATH/git-flow-auto.ps1 -type hotfix -action track -name'" >> $HOME/.bashrc

## PUBLISH SECTION ##
    # git flow publish feature
echo "alias gf-feature-publish='$SCRIPT_PATH/git-flow-auto.ps1 -type feature -action publish -name'" >> $HOME/.bashrc
    # git flow publish release
echo "alias gf-release-publish='$SCRIPT_PATH/git-flow-auto.ps1 -type release -action publish -name'" >> $HOME/.bashrc
    # git flow publish release
echo "alias gf-hotfix-publish='$SCRIPT_PATH/git-flow-auto.ps1 -type hotfix -action publish -name'" >> $HOME/.bashrc

# source the modfied bashrc file
source $HOME/.bashrc
